Dummy bare-metal program to test if the aarch64-unknown-none and 
aarch64-unknown-redox triples are working.

I use the following invocation for a successful build:

$ XARGO_RUST_SRC=/home/robin/work/repos/redox-workspace/redox.new/rust/src/ RUSTUP_TOOLCHAIN=custom xargo rustc --target aarch64-unknown-none
